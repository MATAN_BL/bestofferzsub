package com.company;

import io.vertx.core.Vertx;

import java.io.*;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Vertx vertx = Vertx.vertx();
        Properties prop = new Properties();
        if (args.length > 0 && args[0].equals("--browser")) {
            prop.setProperty("browser", "true");
        } else {
            prop.setProperty("browser", "false");
        }
        readConfFile(prop);
        Trie trieOfCityNames = new Trie(prop);
        vertx.deployVerticle(new VertxHttpServerVerticle(trieOfCityNames, prop));
	}

	private static void readConfFile(Properties prop) throws FileNotFoundException {
        try {
            InputStream input = Main.class.getClassLoader().getResourceAsStream("config.properties");
            prop.load(input);
        } catch (IOException ex) {
            throw new FileNotFoundException();
        }
    }
}

