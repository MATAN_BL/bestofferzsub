package com.company;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;


public class OpenAPIWeatherClientVerticle extends AbstractVerticle {

    private final static int minErrorCodeStatus = 300;
    private final static double kalvinToCelsiusOffset = 273.15;
    private Trie trieOfCityNames;
    private Properties prop;
    private JsonObject weatherForecastObj;
    private HttpServerRequest request;
    private String key;
    private String apiUri;
    private LocalDate currentDate;
    private int numberOfDays;
    private String csvFileName;
    private boolean browserDisplay;

    public OpenAPIWeatherClientVerticle(HttpServerRequest request, Trie trieOfCityNames, Properties prop,
        JsonObject weatherForecastObj) throws FileNotFoundException {
        this.trieOfCityNames = trieOfCityNames;
        this.prop = prop;
        this.weatherForecastObj = weatherForecastObj;
        this.request = request;
        this.key = prop.getProperty("apiKey");
        this.apiUri = prop.getProperty("apiUri");
        this.currentDate = LocalDate.now();
        this.csvFileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + ".csv";
        this.numberOfDays = Integer.parseInt(request.getParam("days"));
        this.browserDisplay = Boolean.valueOf(prop.getProperty("browser"));
    }

    @Override
    public void start() throws Exception {
        HttpClient httpClient = vertx.createHttpClient();
        String city = request.getParam("city").replace("\"", "");
        String country = request.getParam("country");
        String countryAndCityName = String.format("%s;%s", country.toLowerCase(), city.toLowerCase());
        String cityIdCode = trieOfCityNames.convertCityNameToCityNumber(countryAndCityName);
        String uri = String.format(apiUri, cityIdCode, key);

        httpClient.getAbs(uri, httpClientResponse-> {
            if (httpClientResponse.statusCode() > minErrorCodeStatus) {
                System.out.println("Response from api.openweathermap.org fails!");
                System.out.println(httpClientResponse.statusCode() + " " + httpClientResponse.statusMessage());
                return;
            }
            httpClientResponse.bodyHandler(buffer -> {
                System.out.println("Response from api.openweathermap.org is OK");
                JsonArray arr = (JsonArray) new JsonObject(buffer.getString(0, buffer.length())).getValue("list");
                HttpServerResponse response = this.request.response();
                fillWeatherForecastObj(weatherForecastObj, country, city, readDataForNumberOfDays(arr, currentDate));
                if (browserDisplay) {
                    response.sendFile("client/index.html");
                } else {
                    response.headers().add("Content-Disposition", "attachment; filename=" + csvFileName);
                    response.sendFile(csvFileName);
                }
            });
        }).end();
    }

    private JsonObject fillWeatherForecastObj(JsonObject weatherForecastObj, String country, String city, JsonArray weatherPerDays ) {
        weatherForecastObj.put("country", country.toUpperCase());
        weatherForecastObj.put("city", capitalizeFirstLetter(city));
        weatherForecastObj.put("weather", weatherPerDays);
        return weatherForecastObj;
    }

    private String capitalizeFirstLetter(String word) {
        String wordLowerCase = word.toLowerCase();
        return wordLowerCase.substring(0,1).toUpperCase() + wordLowerCase.substring(1,word.length());
    }

    private JsonArray readDataForNumberOfDays(JsonArray arr, LocalDate curDate) {
        JsonArray tempPerDays = new JsonArray();
        for (int i = 0; i < this.numberOfDays; i++) {
            tempPerDays.add(getAverageTempPerADay(curDate.plus(i, ChronoUnit.DAYS).toString(), arr));
        }
        CsvFileWriter csvFileWriter = new CsvFileWriter(tempPerDays, csvFileName, prop);
        csvFileWriter.writeCsvFile();
        return tempPerDays;
    }

    private JsonObject getAverageTempPerADay(String date, JsonArray arr) {
        List<String> tempList = new ArrayList<>();
        List<String> tempMinList = new ArrayList<>();
        List<String> tempMaxList = new ArrayList<>();

        for (int i = 0; i < arr.size(); i++) {
            JsonObject obj = arr.getJsonObject(i);
            if (obj.getValue("dt_txt").toString().contains(date)) {
                tempList.add(String.valueOf(obj.getJsonObject("main").getValue("temp")));
                tempMinList.add(String.valueOf(obj.getJsonObject("main").getValue("temp_min")));
                tempMaxList.add(String.valueOf(obj.getJsonObject("main").getValue("temp_max")));
            }
        }
        JsonObject tempPerDayObj = new JsonObject();
        tempPerDayObj.put("date", date);
        tempPerDayObj.put("temp", getAverageFromListOfStrings(tempList));
        tempPerDayObj.put("temp_min", getAverageFromListOfStrings(tempMinList));
        tempPerDayObj.put("temp_max", getAverageFromListOfStrings(tempMaxList));
        return tempPerDayObj;
    }

    private String getAverageFromListOfStrings(List<String> arr) {
        // reducing 273.15 is converting from Kalvin to Celsius
        return String.format("%.2f", arr.stream().mapToDouble(s -> Double.parseDouble(s) - kalvinToCelsiusOffset).sum() / arr.size());
    }
}