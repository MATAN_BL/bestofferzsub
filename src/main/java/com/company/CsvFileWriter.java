package com.company;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class CsvFileWriter {
    private JsonArray tempPerDays;
    private String csvFileName;
    private Properties prop;

    public CsvFileWriter(JsonArray tempPerDays, String csvFileName, Properties prop) {
        this.tempPerDays = tempPerDays;
        this.csvFileName = csvFileName;
        this.prop = prop;
    }

    public void writeCsvFile() {
        String[] tableTitle = new String[]{prop.getProperty("tableTitle.date"),
                prop.getProperty("tableTitle.dayTemp"), prop.getProperty("tableTitle.minTemp"),
                prop.getProperty("tableTitle.maxTemp")};
        try {
            PrintWriter csvFileHandler = new PrintWriter(new File(this.csvFileName));
            addRowToFile(csvFileHandler, tableTitle);
            for (Object dayObj : tempPerDays) {
                writeDayTempToFile(csvFileHandler, (JsonObject) dayObj);
            }
            csvFileHandler.close();
            System.out.println("File " + csvFileName + " is ready");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void addRowToFile(PrintWriter csvFileHandler, String[] arr) {
        Iterator<String> iterator = Arrays.asList(arr).iterator();
        StringBuilder sb = new StringBuilder();
        sb.append(iterator.next());
        while(iterator.hasNext()) {
            sb.append(',');
            sb.append(iterator.next());
        }
        sb.append("\n");
        csvFileHandler.write(sb.toString());
    }

    private void writeDayTempToFile(PrintWriter csvFileHandler, JsonObject jsonPerDay) {
        String[] values = {jsonPerDay.getString("date"), jsonPerDay.getString("temp"),
            jsonPerDay.getString("temp_min"), jsonPerDay.getString("temp_max") };
        addRowToFile(csvFileHandler, values);
    }

}
