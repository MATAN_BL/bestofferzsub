package com.company;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;

import java.io.FileNotFoundException;
import java.util.Properties;

public class VertxHttpServerVerticle extends AbstractVerticle {

    private static int okStatusCode = 200;
    private static int errorStatusCode = 401;
    private JsonObject weatherForecastObj;
    private Trie trieOfCityNames;
    private Properties prop;
    private String endPointHealthCheck;
    private String endPointHello;
    private String endPointForecastAjax;
    private String endPointForecast;
    private String answerToHealthCheck;
    private int port;

    public VertxHttpServerVerticle(Trie trieOfCityNames, Properties prop) {
        this.trieOfCityNames = trieOfCityNames;
        this.prop = prop;
        this.weatherForecastObj = new JsonObject();
        this.port = Integer.valueOf(prop.getProperty("port"));
        this.endPointHealthCheck = prop.getProperty("endpoint.healthcheck");
        this.endPointHello = prop.getProperty("endpoint.hello");
        this.endPointForecastAjax = prop.getProperty("endpoint.forecastajax");
        this.endPointForecast = prop.getProperty("endpoint.forecast");
        this.answerToHealthCheck = prop.getProperty("answerToHealthCheck");
    }

    @Override
    public void start() throws Exception {
        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(request -> {
            try {
                handleRequest(request, vertx);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        httpServer.listen(port);
    }

    private void handleRequest(HttpServerRequest request, Vertx vertx) throws FileNotFoundException {
        HttpServerResponse response = request.response();
        if (request.method() == HttpMethod.GET) {
            if (request.path().equals(endPointHealthCheck)) {
                returnResponse(response, answerToHealthCheck, true);
                System.out.println("OK");
                return;
            }
            else if (request.path().equals(endPointHello) && (request.getParam("name") != null)) {
                returnResponse(response, "Hello " + request.getParam("name") + "!\n", true);
                return;
            }
            else if (request.path().equals(endPointForecast)) {
                vertx.deployVerticle(new OpenAPIWeatherClientVerticle(request, trieOfCityNames, prop, weatherForecastObj));
                // response will be handles by the class OpenAPIWeatherClientVerticle
                System.out.println("This request is being handled");
                return;
            }
            else if (request.path().equals(endPointForecastAjax)) {
                returnForecastAjaxResponse(request);
                return;
            }
            else {
                response.sendFile("client" + request.path());
                return;
            }
        }
        returnResponse(response, "Not Valid Request", false);
        System.out.println("This request cannot be handled");
    }

    private void returnResponse(HttpServerResponse response, String resBody, boolean isOk) {
        if (isOk) {
            response.setStatusCode(okStatusCode);
        } else {
            response.setStatusCode(errorStatusCode);
        }
        response.headers()
            .add("Content-Length", String.valueOf(resBody.length()))
            .add("Content-Type", "text/html");
        response.write(resBody);
        response.end();
    }

    private void returnForecastAjaxResponse(HttpServerRequest request) {
        HttpServerResponse response = request.response();
        String tempPerDaysString = weatherForecastObj.toString();
        response.headers()
                .add("Content-Length", String.valueOf(tempPerDaysString.length()))
                .add("Content-Type", "text/html; charset=utf-8")
                .add("Access-Control-Allow-Origin", "*");
        response.write(tempPerDaysString);
        response.end();
    }
}
