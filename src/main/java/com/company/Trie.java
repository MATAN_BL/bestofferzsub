package com.company;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

public class Trie {

    private String cityListFilePath;
    private Node head;
    private Properties prop;

    public Trie(Properties prop) {
        this.prop = prop;
        this.cityListFilePath = prop.getProperty("cityListFilePath");
        try {
            this.head = this.createTrie();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Node createTrie() throws FileNotFoundException {

        try (InputStream inputStream = Trie.class.getClassLoader().getResourceAsStream(cityListFilePath)) {
            Node trie = new Node('*', false);
            Scanner read = new Scanner(inputStream);
            read.useDelimiter("\n");
            String line;
            while (read.hasNext()) {
                line = read.next();
                String[] lineArr = line.split("\t");
                String cityId = lineArr[0];
                String cityName = lineArr[1].replaceAll("[\t\r\n]", "").toLowerCase();
                String countryName = lineArr[4].replaceAll("[\t\r\n]", "").toLowerCase();
                String cityNameInput = String.format("%s;%s", countryName, cityName);
                trie.addCity(cityNameInput, cityId);
            }
            return trie;
        } catch (IOException ex) {
            throw new FileNotFoundException();
        }
    }

    public void addCity(String cityNumber) {
        this.head.addCity(cityNumber, cityNumber);
    }

    public String convertCityNameToCityNumber(String cityName) {
        return this.head.convertCityNameToCityNumber(cityName);
    }
}
