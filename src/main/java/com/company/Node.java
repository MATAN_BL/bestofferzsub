package com.company;

import java.util.HashMap;
import java.util.Map;

public class Node {

    private char c;
    private boolean isCity;
    private String cityNumber;
    private Map<Character, Node> nextCharHash;

    public Node(char c, boolean isCity) {
        this.c = c;
        this.isCity = isCity;
        this.nextCharHash = new HashMap<>();
        this.cityNumber = "";
    }

    public void addCity(String suffix, String cityNumber) {
        if (suffix.length() == 0) {
            this.isCity = true;
            this.cityNumber = cityNumber;
            return;
        }
        char addedChar = suffix.charAt(0);
        if (!this.nextCharHash.containsKey(addedChar)) {
            this.nextCharHash.put(addedChar, new Node(addedChar, false));
        }
        this.nextCharHash.get(addedChar).addCity(suffix.substring(1, suffix.length()), cityNumber);
    }

    public String convertCityNameToCityNumber(String cityName) {
        if (cityName.isEmpty()) {
            if (this.isCity) {
                return this.cityNumber;
            } else {
                return null;
            }
        }
        char nextChar = cityName.charAt(0);
        if (this.nextCharHash.containsKey(nextChar)) {
            return this.nextCharHash.get(nextChar).convertCityNameToCityNumber(cityName.substring(1, cityName.length()));
        }
        return null;
    }

    public String getCityNumber() {
        return cityNumber;
    }

    public void setCityNumber(String cityNumber) {
        this.cityNumber = cityNumber;
    }

    public char getC() {
        return c;
    }

    public void setC(char c) {
        this.c = c;
    }

    public boolean isCity() {
        return isCity;
    }

    public void setCity(boolean city) {
        isCity = city;
    }

    public Map<Character, Node> getNextCharHash() {
        return nextCharHash;
    }

    public void setNextCharHash(HashMap<Character, Node> nextCharHash) {
        this.nextCharHash = nextCharHash;
    }
}
